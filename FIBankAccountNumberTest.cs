using System;
using VismaTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VismaTasksTest
{
    [TestClass]
    public class FIBankAccountNumberTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            // Arrange
            FIBankAccountNumber accNum = new FIBankAccountNumber("123456-785"); 
            string expected = "123456-785";
            // Act
            string actual = accNum.BankAccountNumber;
            // Assert
            Assert.AreEqual(expected, actual);

        }
        [TestMethod]
        public void GetLongFormatTest()
        {
            // Arrange
            FIBankAccountNumber accNum = new FIBankAccountNumber("123456-785");
            string expected = "12345600000785";
            // Act
            string actual = accNum.GetLongFormat();
            // Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
